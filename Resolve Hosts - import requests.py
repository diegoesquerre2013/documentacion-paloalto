import requests
import json

url = "https://pan.dev/api/v30.00/hosts/evaluate"

payload = json.dumps({
  "images": [    # Lista de imagen ¿a resolver? ¿que un usuario puede subir?
    {
      "Secrets": [ # Son rutas que estasn introducidos ¿dentro de la imagen?
        "string"
      ],
      "_id": "string", # Identificador de la imagen
      "agentless": True, # indica que el host fue revisado con un escaner de tipo "agentless"
      "allCompliance": { # contiene datos para verificar si cumple con las condiciones apropiadas
        "compliance": [ # son las condiciones que fueron aprobadas
          {
            "applicableRules": [ # Reglas que se aplican para el paquete de datos
              "string"
            ],
            "binaryPkgs": [ # nombres de packages binarios de distribución (packages que se obtienen desde la fuente del package)
              "string"
            ],
            "block": True, # indica si la vulnerabilidad puedes bloquearse (con un True) y si no (con un False)
            "cause": "string", # añade información sobre la causa principal de la vulnerabilidad
            "cri": True, # Indica si hay una vulnerabilidad de CRI (Container Runtime Interface) con un True si existe vulnerabilidad o un False si es caso contrario.
            "custom": True, # Indica si la vulnerabilidad es personalizada con True o False
            "cve": "string", # CVE ID (Common Vulnerabilities and Exposures Identification) de la vulnerabilidad (Si es que hay)
            "cvss": 0, # Common Vulnerability Scoring System (Sistema de puntuación de vulnerabilidades comunes) evalúa la gravedad de dichas vulnerabilidades
            "description": "string", # Describe la vulerabilidad
            "discovered": "2023-04-28T08:01:28.200Z", # indica el momento en que se descubrió la vulnerabilidad
            "exploit": [ # es la fuente de un exploit
              "",
              "exploit-db",
              "exploit-windows",
              "cisa-kev"
            ],
            "exploits": [ # los datos de exploits encontrados para un CVE
              {
                "kind": [ # Tipo de exploit
                  "poc",
                  "in-the-wild"
                ],
                "link": "string", # Enlace a la info sobre el exploit
                "source": [ # Son otras fuentes de un exploit
                  "",
                  "exploit-db",
                  "exploit-windows",
                  "cisa-kev"
                ]
              }
            ],
            "fixDate": 0, # fecha/hora en que se arregló la vulnerabilidad (se mide en tiempo Unix)
            "fixLink": "string", # enlace a la info de la versión fija que el proveedor tiene
            "functionLayer": "string", # Especifica el ID del elemento o capa sin servidor en el que se descubrió la vulnerabilidad.
            "gracePeriodDays": 0, # Número de días de gracia restantes para una vulnerabilidad, según el período de gracia configurado. Nil si no se aplica ninguna regla de vulnerabilidad de bloque.
            "id": 0, # ID de la vulnerabilidad
            "layerTime": 0, # Fecha/hora de la capa de imagen a la que pertenece el CVE (Common Vulnerabilities and Exposures)
            "link": "string", # Enlace del proveedor al CVE (Common Vulnerabilities and Exposures)
            "packageName": "string", # Nombre del package que causó la vulnerabilidad
            "packageVersion": "string", # Versión del paquete que provocó la vulnerabilidad (puede ser nula también)
            "published": 0, # Fecha/hora en que se publicó la vulnerabilidad (se mide en tiempo Unix)
            "riskFactors": {}, # Hace un mapeo de la existencia de factores de riesgo de vulnerabilidad
            "severity": "string", # Representa de forma textual la gravedad de la vulnerabilidad.
            "status": "string", # Estado del proveedor para la vulnerabilidad.
            "templates": [ # Lista de templates con las que está asociada la vulnerabilidad.
              [
                "PCI",
                "HIPAA",
                "NIST SP 800-190",
                "GDPR",
                "DISA STIG"
              ]
            ],
            "text": "string", # Descripción de la vulnerabilidad
            "title": "string", # Título de cumplimiento
            "twistlock": True, # Indica si se trata de una vulnerabilidad específica de Twistlock (True) o no (False).
            "type": [ # Va a indicar el tipo de vulnerabilidad que representa
              "container",
              "image",
              "host_config",
              "daemon_config",
              "daemon_config_files",
              "security_operations",
              "k8s_master",
              "k8s_worker",
              "k8s_federation",
              "linux",
              "windows",
              "istio",
              "serverless",
              "custom",
              "docker_stig",
              "openshift_master",
              "openshift_worker",
              "application_control_linux"
            ],
            "vecStr": "string", # Representación textual de los valores métricos utilizados para puntuar la vulnerabilidad.
            "vulnTagInfos": [ # Info de tags (etiquetas) para la vulnerabilidad
              {
                "color": "string",
                "comment": "string",
                "name": "string"
              }
            ]
          }
        ],
        "enabled": True # Indica si las verificaciones de cumplimiento aprobadas están habilitadas por la política.
      },
      "appEmbedded": True, # Indica que esta ¿imagen? fue escaneada por App-Embedded Defender.

      "applications": [ # Productos en la imagen (¿o también es válido decir que son aplicaciones que irán en ella?).
        {
          "installedFromPackage": True, # Indica que la aplicación se instaló como un paquete de sistema operativo.
          "knownVulnerabilities": 0, # Número total de vulnerabilidades para esta aplicación.
          "layerTime": 0, # ¿Capa de imagen? a la que pertenece la aplicación: tiempo de creación de la capa.
          "name": "string", # Nombre de la aplicación
          "path": "string", # Ruta de la aplicación detectada.
          "service": True, # Indica si la aplicación está instalada como un servicio.
          "version": "string" # Versión de la aplicación
        }
      ],
      "baseImage": "string", # Se utiliza al filtrar las vulnerabilidades por imágenes base.
      "binaries": [ # Binarios en ¿la imagen?
        {
          "altered": True, # Indica si el binario se instaló desde un administrador de packages y se modificó/reemplazó (True) o no (False).
          "cveCount": 0, # Número total de CVE (Common Vulnerabilities and Exposures) para este binario específico.
          "deps": [ # Archivos de packages de terceros que utiliza el binario.
            "string"
          ],
          "functionLayer": "string", # ID de la ¿capa? sin servidor en la que se descubrió el paquete.
          "md5": "string", # Md5 hashset del binario (MD5 (Message Digest Method 5) es un algoritmo hash criptográfico que se utiliza para generar un resumen de 128 bits a partir de una cadena de cualquier longitud. Representa los resúmenes como números hexadecimales de 32 dígitos.)
          "missingPkg": True, # Indica si este binario no está relacionado con ningún paquete (True) o no (False).
          "name": "string", # Nombre del binario
          "path": "string", # Ruta relativa del binario dentro del contenedor.
          "pkgRootDir": "string", # Ruta para buscar packages utilizados por el binario.
          "services": [ # Nombres de los servicios que utilizan el binario.
            "string"
          ],
          "version": "string" # Versión del binario
        }
      ],
      "cloudMetadata": { # CloudMetadata son los metadatos de una instancia que se ejecuta en un proveedor de la nube (Amazon Web Services/Google Cloud Platform/Azure)
        "accountID": "string", # ID de cuenta en la nube.
        "awsExecutionEnv": "string", # Entorno de ejecución de Amazon Web Services (por ejemplo, Amazon EC2/Amazon Fargate).
        "image": "string", # Nombre de la imágen?
        "labels": [ # Etiquetas de metadatos del proveedor de la nube.
          {
            "key": "string", # llave de la equiqueta
            "sourceName": "string", # Nombre de la fuente (por ejemplo, para un espacio de nombres, el nombre de la fuente puede ser 'twistlock').
            "sourceType": [ # Indica la fuente de las etiquetas
              "namespace",
              "deployment",
              "aws",
              "azure",
              "gcp",
              "oci"
            ],
            "timestamp": "2023-04-28T08:01:28.201Z", # Hora en que se obtuvo la etiqueta.
            "value": "string" # Valor de la etiqueta.
          }
        ],
        "name": "string", # Nombre de la instancia
        "provider": [ # Representa al proveedor de la nube
          "aws",
          "azure",
          "gcp",
          "alibaba",
          "oci",
          "others"
        ],
        "region": "string", # Región de la instancia.
        "resourceID": "string", # ID único del recurso
        "resourceURL": "string", # URL definida por el servidor para el recurso
        "type": "string", # Tipo de instancia
        "vmID": "string", # ID de la máquina virtual único de Azure.
        "vmImageID": "string" # Contiene el ID de imágen de la máquina virtual
      },
      "clusterType": [ # Es el tipo de clúster
        "AKS",
        "ECS",
        "EKS",
        "GKE",
        "Kubernetes"
      ],
      "clusters": [ # Nombre de los clústeres
        "string"
      ],
      "collections": [ # Colecciones a las que se aplica este resultado
        "string"
      ],
      "complianceDistribution": { # La distribución cuenta el número de vulnerabilidades por tipo
        "critical": 0,
        "high": 0,
        "low": 0,
        "medium": 0,
        "total": 0
      },
      "complianceIssues": [ # Todos los problemas de cumplimiento.
        {
          "applicableRules": [ # Reglas aplicadas en el package
            "string"
          ],
          "binaryPkgs": [ # Nombres de los packages binarios de distribución (paquetes que se construyen a partir de la fuente del paquete).
            "string"
          ],
          "block": True, # Indica si la vulnerabilidad tiene efecto de bloqueo (True) o no (False)
          "cause": "string", # Información adicional sobre la causa principal de la vulnerabilidad
          "cri": True, # Indica si hay una vulnerabilidad de CRI (Container Runtime Interface) con un True si existe vulnerabilidad o un False si es caso contrario.
          "custom": True, # Indica si la vulnerabilidad es personalizada con True o False
          "cve": "string", # CVE ID (Common Vulnerabilities and Exposures Identification) de la vulnerabilidad (Si es que hay)
          "cvss": 0, # Common Vulnerability Scoring System (Sistema de puntuación de vulnerabilidades comunes) evalúa la gravedad de dichas vulnerabilidades
          "description": "string", # Describe la vulerabilidad
          "discovered": "2023-04-28T08:01:28.201Z", # indica el momento en que se descubrió la vulnerabilidad
          "exploit": [ # es la fuente de un exploit
            "",
            "exploit-db",
            "exploit-windows",
            "cisa-kev"
          ],
          "exploits": [ # Tipo de exploit
            {
              "kind": [
                "poc",
                "in-the-wild"
              ],
              "link": "string", # Enlace a la info sobre el exploit
              "source": [ # Son otras fuentes de un exploit
                "",
                "exploit-db",
                "exploit-windows",
                "cisa-kev"
              ]
            }
          ],
          "fixDate": 0, # fecha/hora en que se arregló la vulnerabilidad (se mide en tiempo Unix)
          "fixLink": "string", # enlace a la info de la versión fija que el proveedor tiene
          "functionLayer": "string", # Especifica el ID del elemento o capa sin servidor en el que se descubrió la vulnerabilidad.
          "gracePeriodDays": 0, # Número de días de gracia restantes para una vulnerabilidad, según el período de gracia configurado. Nil si no se aplica ninguna regla de vulnerabilidad de bloque.
          "id": 0, # ID de la vulnerabilidad
          "layerTime": 0, # Fecha/hora de la capa de imagen a la que pertenece el CVE (Common Vulnerabilities and Exposures)
          "link": "string", # Enlace del proveedor al CVE (Common Vulnerabilities and Exposures)
          "packageName": "string", # Nombre del package que causó la vulnerabilidad
          "packageVersion": "string", # Versión del paquete que provocó la vulnerabilidad (puede ser nula también)
          "published": 0, # Fecha/hora en que se publicó la vulnerabilidad (se mide en tiempo Unix)
          "riskFactors": {}, # Hace un mapeo de la existencia de factores de riesgo de vulnerabilidad
          "severity": "string", # Representa de forma textual la gravedad de la vulnerabilidad.
          "status": "string", # Estado del proveedor para la vulnerabilidad.
          "templates": [ # Lista de templates con las que está asociada la vulnerabilidad.
            [
              "PCI",
              "HIPAA",
              "NIST SP 800-190",
              "GDPR",
              "DISA STIG"
            ]
          ],
          "text": "string",
          "title": "string",
          "twistlock": True,
          "type": [
            "container",
            "image",
            "host_config",
            "daemon_config",
            "daemon_config_files",
            "security_operations",
            "k8s_master",
            "k8s_worker",
            "k8s_federation",
            "linux",
            "windows",
            "istio",
            "serverless",
            "custom",
            "docker_stig",
            "openshift_master",
            "openshift_worker",
            "application_control_linux"
          ],
          "vecStr": "string",
          "vulnTagInfos": [
            {
              "color": "string",
              "comment": "string",
              "name": "string"
            }
          ]
        }
      ],
      "complianceIssuesCount": 0,
      "complianceRiskScore": 0,
      "creationTime": "2023-04-28T08:01:28.201Z",
      "distro": "string",
      "ecsClusterName": "string",
      "err": "string",
      "externalLabels": [
        {
          "key": "string",
          "sourceName": "string",
          "sourceType": [
            "namespace",
            "deployment",
            "aws",
            "azure",
            "gcp",
            "oci"
          ],
          "timestamp": "2023-04-28T08:01:28.201Z",
          "value": "string"
        }
      ],
      # Archivos
      "files": [
        {
          "md5": "string",
          "path": "string",
          "sha1": "string",
          "sha256": "string"
        }
      ],
      "firewallProtection": {
        "enabled": True,
        "outOfBandMode": [
          "",
          "Observation",
          "Protection"
        ],
        "ports": [
          0
        ],
        "supported": True,
        "tlsPorts": [
          0
        ],
        "unprotectedProcesses": [
          {
            "port": 0,
            "process": "string",
            "tls": True
          }
        ]
      },
      "firstScanTime": "2023-04-28T08:01:28.201Z",
      "history": [
        {
          "baseLayer": True,
          "created": 0,
          "emptyLayer": True,
          "id": "string",
          "instruction": "string",
          "sizeBytes": 0,
          "tags": [
            "string"
          ],
          "vulnerabilities": [
            {
              "applicableRules": [
                "string"
              ],
              "binaryPkgs": [
                "string"
              ],
              "block": True,
              "cause": "string",
              "cri": True,
              "custom": True,
              "cve": "string",
              "cvss": 0,
              "description": "string",
              "discovered": "2023-04-28T08:01:28.201Z",
              "exploit": [
                "",
                "exploit-db",
                "exploit-windows",
                "cisa-kev"
              ],
              "exploits": [
                {
                  "kind": [
                    "poc",
                    "in-the-wild"
                  ],
                  "link": "string",
                  "source": [
                    "",
                    "exploit-db",
                    "exploit-windows",
                    "cisa-kev"
                  ]
                }
              ],
              "fixDate": 0,
              "fixLink": "string",
              "functionLayer": "string",
              "gracePeriodDays": 0,
              "id": 0,
              "layerTime": 0,
              "link": "string",
              "packageName": "string",
              "packageVersion": "string",
              "published": 0,
              "riskFactors": {},
              "severity": "string",
              "status": "string",
              "templates": [
                [
                  "PCI",
                  "HIPAA",
                  "NIST SP 800-190",
                  "GDPR",
                  "DISA STIG"
                ]
              ],
              "text": "string",
              "title": "string",
              "twistlock": True,
              "type": [
                "container",
                "image",
                "host_config",
                "daemon_config",
                "daemon_config_files",
                "security_operations",
                "k8s_master",
                "k8s_worker",
                "k8s_federation",
                "linux",
                "windows",
                "istio",
                "serverless",
                "custom",
                "docker_stig",
                "openshift_master",
                "openshift_worker",
                "application_control_linux"
              ],
              "vecStr": "string",
              "vulnTagInfos": [
                {
                  "color": "string",
                  "comment": "string",
                  "name": "string"
                }
              ]
            }
          ]
        }
      ],
      # ID del Host
      "hostDevices": [
        {
          "ip": "string",
          "name": "string"
        }
      ],
      "hostRuntimeEnabled": True,
      "hostname": "string",
      "hosts": {},
      "id": "string",
      "image": {
        "created": "2023-04-28T08:01:28.202Z",
        "entrypoint": [
          "string"
        ],
        "env": [
          "string"
        ],
        "healthcheck": True,
        "history": [
          {
            "baseLayer": True,
            "created": 0,
            "emptyLayer": True,
            "id": "string",
            "instruction": "string",
            "sizeBytes": 0,
            "tags": [
              "string"
            ],
            # Vulnerabilidades
            "vulnerabilities": [
              {
                "applicableRules": [
                  "string"
                ],
                "binaryPkgs": [
                  "string"
                ],
                "block": True,
                "cause": "string",
                "cri": True,
                "custom": True,
                "cve": "string",
                "cvss": 0,
                "description": "string",
                "discovered": "2023-04-28T08:01:28.202Z",
                "exploit": [
                  "",
                  "exploit-db",
                  "exploit-windows",
                  "cisa-kev"
                ],
                "exploits": [
                  {
                    "kind": [
                      "poc",
                      "in-the-wild"
                    ],
                    "link": "string",
                    "source": [
                      "",
                      "exploit-db",
                      "exploit-windows",
                      "cisa-kev"
                    ]
                  }
                ],
                "fixDate": 0,
                "fixLink": "string",
                "functionLayer": "string",
                "gracePeriodDays": 0,
                "id": 0,
                "layerTime": 0,
                "link": "string",
                "packageName": "string",
                "packageVersion": "string",
                "published": 0,
                "riskFactors": {},
                "severity": "string",
                "status": "string",
                "templates": [
                  [
                    "PCI",
                    "HIPAA",
                    "NIST SP 800-190",
                    "GDPR",
                    "DISA STIG"
                  ]
                ],
                "text": "string",
                "title": "string",
                "twistlock": True,
                "type": [
                  "container",
                  "image",
                  "host_config",
                  "daemon_config",
                  "daemon_config_files",
                  "security_operations",
                  "k8s_master",
                  "k8s_worker",
                  "k8s_federation",
                  "linux",
                  "windows",
                  "istio",
                  "serverless",
                  "custom",
                  "docker_stig",
                  "openshift_master",
                  "openshift_worker",
                  "application_control_linux"
                ],
                "vecStr": "string",
                "vulnTagInfos": [
                  {
                    "color": "string",
                    "comment": "string",
                    "name": "string"
                  }
                ]
              }
            ]
          }
        ],
        "id": "string",
        "labels": {},
        "layers": [
          "string"
        ],
        "os": "string",
        "repoDigest": [
          "string"
        ],
        "repoTags": [
          "string"
        ],
        "user": "string",
        "workingDir": "string"
      },
      # Productos Instalados
      "installedProducts": {
        "agentless": True,
        "apache": "string",
        "awsCloud": True,
        "crio": True,
        "docker": "string",
        "dockerEnterprise": True,
        "hasPackageManager": True,
        "k8sApiServer": True,
        "k8sControllerManager": True,
        "k8sEtcd": True,
        "k8sFederationApiServer": True,
        "k8sFederationControllerManager": True,
        "k8sKubelet": True,
        "k8sProxy": True,
        "k8sScheduler": True,
        "kubernetes": "string",
        "openshift": True,
        "openshiftVersion": "string",
        "osDistro": "string",
        "serverless": True,
        "swarmManager": True,
        "swarmNode": True
      },
      # Instancias
      "instances": [
        {
          "host": "string",
          "image": "string",
          "modified": "2023-04-28T08:01:28.202Z",
          "registry": "string",
          "repo": "string",
          "tag": "string"
        }
      ],
      "isARM64": True,
      "k8sClusterAddr": "string",
      "labels": [
        "string"
      ],
      "layers": [
        "string"
      ],
      "missingDistroVulnCoverage": True,
      "namespaces": [
        "string"
      ],
      "osDistro": "string",
      "osDistroRelease": "string",
      "osDistroVersion": "string",
      "packageCorrelationDone": True,
      "packageManager": True,
      "packages": [
        {
          "pkgs": [
            {
              "binaryIdx": [
                0
              ],
              "binaryPkgs": [
                "string"
              ],
              "cveCount": 0,
              "files": [
                {
                  "md5": "string",
                  "path": "string",
                  "sha1": "string",
                  "sha256": "string"
                }
              ],
              "functionLayer": "string",
              "goPkg": True,
              "jarIdentifier": "string",
              "layerTime": 0,
              "license": "string",
              "name": "string",
              "osPackage": True,
              "path": "string",
              "version": "string"
            }
          ],
          "pkgsType": [
            "nodejs",
            "gem",
            "python",
            "jar",
            "package",
            "windows",
            "binary",
            "nuget",
            "go"
          ]
        }
      ],
      "pullDuration": 0,
      "pushTime": "2023-04-28T08:01:28.202Z",
      "registryNamespace": "string",
      "registryType": "string",
      "repoDigests": [
        "string"
      ],
      "repoTag": {
        "digest": "string",
        "id": "string",
        "registry": "string",
        "repo": "string",
        "tag": "string"
      },
      "rhelRepos": [
        "string"
      ],
      # Factores de riesgo
      "riskFactors": {},
      "scanBuildDate": "string",
      "scanDuration": 0,
      "scanID": 0,
      "scanTime": "2023-04-28T08:01:28.202Z",
      "scanVersion": "string",
      "startupBinaries": [
        {
          "altered": True,
          "cveCount": 0,
          "deps": [
            "string"
          ],
          "functionLayer": "string",
          "md5": "string",
          "missingPkg": True,
          "name": "string",
          "path": "string",
          "pkgRootDir": "string",
          "services": [
            "string"
          ],
          "version": "string"
        }
      ],
      "stopped": True,
      "tags": [
        {
          "digest": "string",
          "id": "string",
          "registry": "string",
          "repo": "string",
          "tag": "string"
        }
      ],
      "topLayer": "string",
      "trustResult": {
        "groups": [
          {
            "_id": "string",
            "disabled": True,
            "images": [
              "string"
            ],
            "layers": [
              "string"
            ],
            "modified": "2023-04-28T08:01:28.203Z",
            "name": "string",
            "notes": "string",
            "owner": "string",
            "previousName": "string"
          }
        ],
        "hostsStatuses": [
          {
            "host": "string",
            "status": [
              "trusted",
              "untrusted"
            ]
          }
        ]
      },
      "trustStatus": [
        "trusted",
        "untrusted"
      ],
      # Bloquear/Liberar imagen
      "twistlockImage": True,
      "type": [
        "image",
        "ciImage",
        "container",
        "host",
        "agentlessHost",
        "registry",
        "serverlessScan",
        "ciServerless",
        "vm",
        "tas",
        "ciTas",
        "cloudDiscovery",
        "serverlessRadar",
        "serverlessAutoDeploy",
        "hostAutoDeploy",
        "codeRepo",
        "ciCodeRepo"
      ], # Vulnerabilidades N° 2
      "vulnerabilities": [
        {
          "applicableRules": [
            "string"
          ],
          "binaryPkgs": [
            "string"
          ],
          "block": True,
          "cause": "string",
          "cri": True,
          "custom": True,
          "cve": "string",
          "cvss": 0,
          "description": "string",
          "discovered": "2023-04-28T08:01:28.203Z",
          "exploit": [
            "",
            "exploit-db",
            "exploit-windows",
            "cisa-kev"
          ],
          "exploits": [
            {
              "kind": [
                "poc",
                "in-the-wild"
              ],
              "link": "string",
              "source": [
                "",
                "exploit-db",
                "exploit-windows",
                "cisa-kev"
              ]
            }
          ],
          "fixDate": 0,
          "fixLink": "string",
          "functionLayer": "string",
          "gracePeriodDays": 0,
          "id": 0,
          "layerTime": 0,
          "link": "string",
          "packageName": "string",
          "packageVersion": "string",
          "published": 0,
          "riskFactors": {},
          "severity": "string",
          "status": "string",
          "templates": [
            [
              "PCI",
              "HIPAA",
              "NIST SP 800-190",
              "GDPR",
              "DISA STIG"
            ]
          ], # Bloquear/Liberar texto
          "text": "string",
          "title": "string",
          "twistlock": True,
          "type": [
            "container",
            "image",
            "host_config",
            "daemon_config",
            "daemon_config_files",
            "security_operations",
            "k8s_master",
            "k8s_worker",
            "k8s_federation",
            "linux",
            "windows",
            "istio",
            "serverless",
            "custom",
            "docker_stig",
            "openshift_master",
            "openshift_worker",
            "application_control_linux"
          ],
          "vecStr": "string",
          "vulnTagInfos": [
            {
              "color": "string",
              "comment": "string",
              "name": "string"
            }
          ]
        }
      ],
      # Contador de Vulnerabilidades
      "vulnerabilitiesCount": 0,
      "vulnerabilityDistribution": {
        "critical": 0,
        "high": 0,
        "low": 0,
        "medium": 0,
        "total": 0
      },
      "vulnerabilityRiskScore": 0,
      "wildFireUsage": {
        "bytes": 0,
        "queries": 0,
        "uploads": 0
      }
    }
  ]
})

headers = {
  'Content-Type': 'application/json',
  'Accept': 'application/json'
}

response = requests.request("POST", url, headers=headers, data=payload)

print(response.text)